import json
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateTimeEncoder(json.JSONEncoder):
    #set default
    def default(self, o):
        #if o is an instance of datetime
        if isinstance(o, datetime):
            #return the formatted date
            return o.isoformat()
        else:
            #return super class default method
            return super().default(o)


#?this will be used for every encoder in views
class ModelEncoder(QuerySetEncoder, DateTimeEncoder, json.JSONEncoder):
    encoders = {}

    def default(self, o):
        #if o is an instance of model
        if isinstance(o, self.model):
            #create a d var set to {}
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, 'get_api_url'):
            #    then add its return value to the dictionary
            #    with the key "href"
                d['href'] = o.get_api_url()
            #iterate over properties list
            #!self.properties is coming from DetEncoders in list of props
            for property in self.properties:
                #get the valof the property instance
                #!we can get the val of the prop by its name with this
                val = getattr(o, property)
                #if there is a property in encoders
                if property in self.encoders:
                    #set encoder to the property
                    encoder = self.encoders[property]
                    #set value to default encoder val
                    val = encoder.default(val)
                #add the property name as key to the d var
                d[property] = val
                #add it to the d var with the property name as key
            d.update(self.get_extra_data(o))
            #return d
            return d
        else:
            #return super class default method
            return super().default(o)

    def get_extra_data(self, o):
        return {}
